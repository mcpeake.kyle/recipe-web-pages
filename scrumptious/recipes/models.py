from django.db import models


# any changes, need to "python models.py makemigratoins", "python models.py migrate" to update DataBase

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=100)
    picture = models.URLField(max_length=400, blank=True)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

class Post(models.Model):
    title = models.CharField(max_length=100)
    summary = models.CharField(max_length=1000)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    is_published = models.BooleanField(default=False)

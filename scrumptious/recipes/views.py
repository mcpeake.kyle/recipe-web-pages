from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.

def create_recipe(request):
    if request.method == "POST": # If the form has been submitted...
        form = RecipeForm(request.POST) # A form bound to POST data
        if form.is_valid(): # All validation rules pass
            form.save() # Saves form to the database
            return redirect("recipe_list") # Redirects to recipe_list in urls
    else: # ... form has not been submitted
        print("view function create_recipe else statement occurred")
        form = RecipeForm() # show user the same form
    context = {
        "recipe_form": form, # Builds context dictionary to pass to the template
    }
    return render(request, "recipes/create.html", context) # render the template


def edit_recipe(request, id):
    recipe = Recipe.objects.get(id=id)
    # recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "recipe_object": recipe,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)


def show_recipe(request, id): # For list.html
    recipe = get_object_or_404(Recipe, id=id)
# if going to have an empty page at some point of process,
# better to use recipes = Recipe.objects.all() else it will 404
    context = {
        "recipe_object" : recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request): # For detail.html
    recipes = Recipe.objects.all()
    context = {
        "recipe_list" : recipes,
    }
    return render(request, "recipes/list.html", context)





# def show_recipe(request):
#     return render(request, "recipes/detail.html")

# def home(request):
#     return render(request, "recipes/list.html")
